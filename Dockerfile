# https://hub.docker.com/_/ruby
FROM ruby:3.0.5

# Install bundler
RUN gem update --system 3.2.33
RUN gem install bundler

# Directory within container
WORKDIR /usr/src/app

# Install production dependencies.
COPY Gemfile Gemfile.lock ./
# ENV BUNDLE_FROZEN=true # --> Will prevent Gemfile.lock to be reconstructed at build time
RUN bundle install

# Copy local code to the container image.
COPY . ./

# Expose port 3000
EXPOSE 3000

# Run the web service on container startup.
CMD ["bash", "entrypoint.sh"]
#CMD [ "rails","server","-b","0.0.0.0" ]